# Copyright 2014 SolidBuilds.com. All rights reserved
#
# Authors: Ling Thio <ling.thio@gmail.com>


from flask import render_template, flash, request, redirect, url_for
from flask_user import login_required, roles_required
from werkzeug import secure_filename
from flask.ext.login import current_user

from app.app_and_db import app, db
import os
import subprocess
from app.users.models import User

# The Home page is accessible to anyone
@app.route('/')
def home():
	return render_template('pages/home.html')
   
@app.route('/submit', methods=["GET","POST"])
@login_required
def submit():
        if request.method=="GET":
                return render_template('pages/submit.html')
        elif request.method=="POST":
                return render_template('pages/submit.html')

@app.route('/submit2', methods=["GET","POST"])
@login_required
def submit2():
        if request.method=="GET":
                return render_template('pages/submit_flow2.html')
        elif request.method=="POST":
                return render_template('pages/submit_flow2.html')

@app.route('/stereo')
def stereo():
        users=User.query.filter(User.stereo_score_all.isnot(None)).all()
        if users:
                return render_template('pages/stereo.html',users=users)
        else:
                return render_template('pages/stereo.html',msg='there are no users',title='Empty')

@app.route('/flow')
def flow():
        users=User.query.filter(User.flow_score_all.isnot(None)).all()
        if users:
                return render_template('pages/flow.html',users=users)
        else:
                return render_template('pages/flow.html',msg='there are no users',title='Empty')

@app.route('/flow2')
def flow2():
        users=User.query.filter(User.flow_score_all.isnot(None)).all()
        if users:
                return render_template('pages/flow2.html',users=users)
        else:
                return render_template('pages/flow2.html',msg='there are no users',title='Empty')


@app.route('/stereo_submit')
def stereo_submit():
        return render_template('pages/stereo_submit.html')

@app.route('/upload',methods=['POST'])
def upload():
        file=request.files['file']
        mname=request.form.get('mname')
        if file:
                current_user.name=mname
                db.session.commit()
                filename=secure_filename(file.filename)
                directory=os.path.join(app.config['UPLOAD_FOLDER'],current_user.user_auth.email)
                if not os.path.exists(directory):
                        os.makedirs(directory)
                file.save(os.path.join(directory,filename))
                cmd = ["app/WebRun",directory,"app/data",current_user.user_auth.email]
                #p = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
                p=subprocess.Popen(' '.join(cmd), shell=True, executable='/bin/bash')
                return render_template('pages/submit.html')
        return render_template('pages/submit.html')

